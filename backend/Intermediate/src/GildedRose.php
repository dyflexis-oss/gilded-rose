<?php

namespace Dyflexis\Applicants;

class GildedRose
{
    public $name;

    public $quality;

    public $sellIn;

    public function __construct($name, $quality, $sellIn)
    {
        $this->name = $name;
        $this->quality = $quality;
        $this->sellIn = $sellIn;
    }

    public static function of($name, $quality, $sellIn) {
        return new static($name, $quality, $sellIn);
    }

    public function tick()
    {
        if ($this->name != 'Aged Brie') {
            if ($this->quality > 0) {
                $this->quality = $this->quality - 1;
            }
        } else {
            if ($this->quality < 50) {
                $this->quality = $this->quality + 1;
            }
        }

        $this->sellIn = $this->sellIn - 1;

        if ($this->sellIn < 0) {
            if ($this->name != 'Aged Brie') {
                if ($this->quality > 0) {
                    $this->quality = $this->quality - 1;
                }
            } else {
                if ($this->quality < 50) {
                    $this->quality = $this->quality + 1;
                }
            }
        }
    }
}